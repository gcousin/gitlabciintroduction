#!/bin/bash

# Docker for dev
docker build -f Dockerfile_cxx -t registry.gitlab.inria.fr/formations/integrationcontinue/gitlabciintroduction/cxx .
docker build -f Dockerfile_python -t registry.gitlab.inria.fr/formations/integrationcontinue/gitlabciintroduction/python .
docker build -f Dockerfile_java -t registry.gitlab.inria.fr/formations/integrationcontinue/gitlabciintroduction/java .

# Docker for build
docker build -f Dockerfile_cxx_build -t registry.gitlab.inria.fr/formations/integrationcontinue/gitlabciintroduction/cxx_build .
docker build -f Dockerfile_python_build -t registry.gitlab.inria.fr/formations/integrationcontinue/gitlabciintroduction/python_build .
docker build -f Dockerfile_java_build -t registry.gitlab.inria.fr/formations/integrationcontinue/gitlabciintroduction/java_build .

docker login registry.gitlab.inria.fr

# Docker for dev
docker push registry.gitlab.inria.fr/formations/integrationcontinue/gitlabciintroduction/cxx
docker push registry.gitlab.inria.fr/formations/integrationcontinue/gitlabciintroduction/python
docker push registry.gitlab.inria.fr/formations/integrationcontinue/gitlabciintroduction/java

# Docker for build
docker push registry.gitlab.inria.fr/formations/integrationcontinue/gitlabciintroduction/cxx_build
docker push registry.gitlab.inria.fr/formations/integrationcontinue/gitlabciintroduction/python_build
docker push registry.gitlab.inria.fr/formations/integrationcontinue/gitlabciintroduction/java_build
