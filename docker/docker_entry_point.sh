#!/bin/bash

USER_ID=${LOCAL_USER_ID:-9001}

# Add user and silent warning
useradd --shell /bin/bash -u $USER_ID -o -c "" -m user  &>/dev/null

export HOME=/home/user

exec /usr/local/bin/gosu user "$@"
