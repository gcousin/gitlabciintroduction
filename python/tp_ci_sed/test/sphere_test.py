# -*- coding:utf-8 -*-

import pytest
from tp_ci_sed import sphere

def test_volume_0():
    my_sphere = sphere.Sphere(0.0)
    vol = my_sphere.volume()

    assert 0.0 == vol
