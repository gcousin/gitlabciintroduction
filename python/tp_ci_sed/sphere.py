# -*- coding:utf-8 -*-

import argparse
import math


class Sphere(object):

    def __init__(self, radius):
        self.radius = radius

    def __str__(self):
        return '%s(%f)' % (self.__class__.__name__, self.radius)

PARSER = argparse.ArgumentParser(description='Sphere volume computation')
PARSER.add_argument('radius', type=float, help='Sphere radius')


def main():
    """ Compute sphere volume """
    opts = PARSER.parse_args()

    my_sphere = Sphere(opts.radius)
    volume = my_sphere.volume()
    message = str(my_sphere) + " volume is " + repr(volume)
    print(message)

