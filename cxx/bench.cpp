/**
 * This file is a part of TPCI_SEDRA project.
 * Copyright (c) INRIA 2015
 *
 * This project is headed by SED Rhone-Alpes service at INRIA.
 *
 */
#include "Sphere.hpp"
#include <chrono>
#include <iostream>

int main()
{

  double result=0.;
  double etime=0.;

  std::chrono::time_point<std::chrono::system_clock> start, end;

  // useless
  unsigned nbtrials=10;

  std::cout.precision(5);
  std::cout << std::scientific;

  for (unsigned trial=0; trial<nbtrials; ++trial)
  {

    start = std::chrono::system_clock::now();

    for(std::size_t i=0; i<1000000; ++i)
    {
      result+=Sphere((double)(i)).volume();
    }

    end = std::chrono::system_clock::now();
    etime += std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
  }

  etime /= nbtrials;

  std::cout << "time:" << etime <<std::endl;
  std::cout << "result:" << result << std::endl;

  return result==0.;
}
