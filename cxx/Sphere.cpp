/**
 * This file is a part of TPCI_SEDRA project.
 * Copyright (c) INRIA 2015
 *
 * This project is headed by SED Rhone-Alpes service at INRIA.
 *
 */
#include "Sphere.hpp"

#include <math.h>

double Sphere::volume() const
{
  return 4 * M_PI * pow(this->radius_, 3) / 3;
}
