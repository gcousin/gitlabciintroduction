/**
 * This file is a part of TPCI_SEDRA project.
 * Copyright (c) INRIA 2015
 *
 * This project is headed by SED Rhone-Alpes service at INRIA.
 *
 */
#ifndef Sphere_hpp
#define Sphere_hpp
class Sphere
{
public:
  Sphere(const double& radius) : radius_(radius) {};

  double volume () const;

private:
  double radius_;
};
#endif
