#
# This file is a part of TPCI_SEDRA project.
# Copyright (c) INRIA 2015
#
# This project is headed by SED Rhone-Alpes service at INRIA.
#
include(TestCXXAcceptsFlag)

# add a compiler flag only if it is accepted
macro(add_cxx_compiler_flag _flag)
  string(REPLACE "-" "_" _flag_var ${_flag})
  check_cxx_accepts_flag("${_flag}" CXX_COMPILER_${_flag_var}_OK)
  if(CXX_COMPILER_${_flag_var}_OK)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${_flag}")
  endif()
endmacro()

# needed for coverage 
find_program(LCOV lcov)
find_program(GENHTML genhtml)

# targets for coverage
add_custom_target(check 
  COMMAND ${CMAKE_COMMAND} --build ${CMAKE_BINARY_DIR} --target test)

add_custom_target(coverage) 

function(add_test_with_coverage name exec arg)
  string(REPLACE ":" "_" _tname ${arg})
  add_test(${name} ${exec} ${arg})
  add_custom_target(${_tname}.info
    COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_SOURCE_DIR}/coverage
    COMMAND ${LCOV} --base-directory ${CMAKE_SOURCE_DIR} --directory ${CMAKE_BINARY_DIR} --capture --output-file ${_tname}.info --no-external
    COMMAND ${GENHTML} -o ${CMAKE_SOURCE_DIR}/coverage/${_tname} ${_tname}.info
    COMMENT "Coverage report is generated in folder ${CMAKE_SOURCE_DIR}/coverage/${_tname}"
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})
  add_dependencies(${_tname}.info check)
  add_dependencies(coverage ${_tname}.info)
endfunction()

