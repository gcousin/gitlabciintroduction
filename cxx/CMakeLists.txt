#
# This file is a part of TPCI_SEDRA project.
# Copyright (c) INRIA 2015
#
# This project is headed by SED Rhone-Alpes service at INRIA.
#

# A CMakeList.txt example for https:://ci.inria.fr tutorial

# A minimum version for cmake command is required.
cmake_minimum_required(VERSION 3.1)

set (CMAKE_CXX_STANDARD 11)

# The tpsed shared library: an unique source file, version number 1
add_library(tpsed SHARED Sphere.cpp)
set_target_properties(tpsed PROPERTIES SOVERSION 1)

# The bench executable is linked with the tpsed
add_executable(bench bench.cpp)
target_link_libraries(bench tpsed)

# tests specifications
# see tests/CMakeLists.txt for details
enable_testing()
add_subdirectory(tests)
