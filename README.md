The INRIA continuous integration practical tutorial will guide you through exercises on a way to make static and dynamic tests
on a simple piece of code in order to improve code quality. These tests will be run locally in a Docker container,
and then on a GitLab CI / CD instance (in order to present the results).

This tutorial has been done by Maurice Brémond, Gaëtan Harter and David Parsons on project https://gforge.inria.fr/projects/tpcisedra - SED INRIA Rhône-Alpes.
This project has been adapted to gitlab, gitlab-ci and to the use of Docker containers by Vincent Rouvreau - SED INRIA Saclay.

For this tutorial, you will have to create an [INRIA gitlab](https://gitlab.inria.fr/formations/integrationcontinue/gitlabciintroduction/wikis/account_gitlab)
and to ask the instructor to be added to the corresponding project.

Docker installation is also required. If you are not familiar with installations, please ask your administrator. Docker images will be accessible with all the tools you will need for the session.

Please follow these steps carefully before joining us to the next tutorial session. 

+ Practical tutorial from a given piece of code (2 h 30 mn):
   * [C++](https://gitlab.inria.fr/formations/integrationcontinue/gitlabciintroduction/wikis/cxx/01_preamble)
   * [Java](https://gitlab.inria.fr/formations/integrationcontinue/gitlabciintroduction/wikis/java/01_preamble)
   * [Python](https://gitlab.inria.fr/formations/integrationcontinue/gitlabciintroduction/wikis/python/01_preamble)

Some advised literature:
 + [Continuous integration by Martin Fowler](http://martinfowler.com/articles/continuousIntegration.html)
 + [Continuous integration on Wikipedia](https://en.wikipedia.org/wiki/Continuous_integration)

Some [notes for the instructors](https://gitlab.inria.fr/formations/integrationcontinue/gitlabciintroduction/wikis/notes_for_instructors).
